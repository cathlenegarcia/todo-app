# ToDo App

This is an API for CRUD operations of a ToDo application.

## Dependencies and Installation

This application requires you to install the following dependencies:

- [Ruby v2.6.3](https://www.ruby-lang.org/en/downloads/)

- [Bundler v1.17.2](https://bundler.io/)

- [Rails v6.0.3.5](https://edgeguides.rubyonrails.org/6_0_release_notes.html)

- PostgreSQL v11.8

Here is a guide for installing the dependencies stated above, refer to the documentations linked for each when you encounter problems in installing.

- Install Homebrew (if you haven't already)

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

- Install RVM (this takes a while)

```
$ echo "gem: --no-document" >> ~/.gemrc
$ curl -L https://get.rvm.io | bash -s stable --auto-dotfiles --autolibs=enable --rails
$ rvm install 2.6.3
$ rvm use 2.6.3 --default
```

- Install PostgreSQL

```
$ brew install postgres
$ brew tap homebrew/services
$ brew services start postgresql
```

- Clone repository

```
$ git clone https://cathlenegarcia@bitbucket.org/cathlenegarcia/todo-app.git
$ cd todo-app
```

- Configure App (see [Configuration](#configuration))

- Install the rest of the required gems

```
$ bundle install
```

- Setup database

```
$ bundle exec rails db:setup   # if database does not exist
$ bundle exec rails db:migrate  # if database already exists
$ bundle exec rails db:seed     # load seed to the database
```
  
  
## Run Rails
In order to run in development:

Use the .env.example file to find the attributes you need to setup your own .env file
Run the following commands to get setup in development and run the server

```
$ bundle exec rails s
```

App will be accessible via `localhost:3000`

## Run Tests
```
$ bundle exec rspec spec
```

## Documentation
Documentation for the available APIs are accessible thru:

```
http://localhost:3000/api-docs/index.html
```

### Configuration
__config/database.yml__

- Create the application config file (`config/database.yml`).
- Refer to the table below for the configuration details

    | Field                       | Description                               |
    | ----------------------------| ------------------------------------------|
    | `adapter`                   | Database adapter                          |
    | `encoding`                  | Database encoding                         |
    | `pool`                      | Number of thread access to DB connections |
    | `timeout`                   | Database timeout (in milliseconds)        |
    | `host`                      | Database host                             |
    | `username`                  | Database credential                       |
    | `password`                  | Database credential                       |
    
    
