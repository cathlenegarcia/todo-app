FactoryBot.define do
  factory :todo_list do
    name { 'ToDo' }
    project { create(:project) }
  end
end
