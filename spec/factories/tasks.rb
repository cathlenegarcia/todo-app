FactoryBot.define do
  factory :task do
    title { 'Title' }
    description { 'Description' }
    todo_list { create(:todo_list) }
  end
end
