FactoryBot.define do
  factory :project do
    name { 'Project Name' }
    user { create(:user) }
  end
end
