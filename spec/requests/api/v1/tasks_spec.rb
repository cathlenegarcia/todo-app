require 'swagger_helper'

RSpec.describe 'Tasks API', type: :request do
  path '/api/v1/todo_lists/{todo_list_id}/tasks' do
    parameter name: :todo_list_id, in: :path, type: :integer, description: 'todo_list_id'
    post 'creates a task' do
      tags 'Task'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :task, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string, description: 'Title of task' },
          description: { type: :string, description: 'Description of task' }
        }
      }

      response '200', 'task created' do
        schema '$ref' => '#/components/schemas/task_created_model'
        let(:todo_list_id) { create(:todo_list).id }
        let(:task) do
          {
            title: 'Title',
            description: 'Description'
          }
        end

        run_test!
      end

      response '404', 'todo list not found' do
        schema '$ref' => '#/components/schemas/todo_list_not_found_error'
        let(:todo_list_id) { 100 }
        let(:task) do
          {
            title: 'Title',
            description: 'Description'
          }
        end
        run_test!
      end
    end

    get 'retrieves tasks' do
      tags 'Task'
      consumes 'application/json'
      produces 'application/json'

      response '200', 'tasks retrieved' do
        schema type: :array,
               collection_format: :csv,
               items: {
                type: 'object',
                properties: {
                  title: { type: :string },
                  description: { type: :string },
                  created_at: { type: :string, format: 'date-time' },
                  updated_at: { type: :string, format: 'date-time' },
                  position: { type: :integer },
                  todo_list_id: { type: :integer}
                }
               }
        let(:todo_list_id) { create(:todo_list).id }
        let!(:tasks) { 
          5.times.each do |_|
            create(:task, todo_list_id: todo_list_id)
          end
        }
        run_test! do |response|
          response_body = JSON.parse(response.body)
          expect(response_body.count).to eq(5)
          expect(response_body.pluck('position')).to eq([1, 2, 3, 4, 5])
        end
      end

      response '404', 'todo list not found' do
        schema '$ref' => '#/components/schemas/todo_list_not_found_error'
        let(:todo_list_id) { 100 }
        let(:task) do
          {
            title: 'Title',
            description: 'Description'
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/todo_lists/{todo_list_id}/tasks/{id}' do
    parameter name: :todo_list_id, in: :path, type: :integer, description: 'todo_list_id'
    parameter name: :id, in: :path, type: :integer, description: 'task id'

    put 'updates a task' do
      tags 'Task'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :task_updates, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string, description: 'Title of task' },
          description: { type: :string, description: 'Description of task' },
          position: { type: :integer, description: 'Position of the task in the ToDo List'}
        }
      }

      response '200', 'task updated' do
        schema '$ref' => '#/components/schemas/task_created_model'
        let(:todo_list) { create(:todo_list) }
        let(:todo_list_id) { todo_list.id }
        let(:task) { create(:task, todo_list_id: todo_list_id) }
        let(:id) { task.id }
        let(:task_updates) do
          {
            title: 'New Title',
            description: 'Description',
            position: 3
          }
        end

        run_test! do |_|
          task.reload
          expect(task.position).to eq(3)
          expect(task.title).to eq('New Title')
          expect(task.description).to eq('Description')
        end
      end

      response '404', 'task not found' do
        schema '$ref' => '#/components/schemas/task_not_found_error'
        let(:todo_list_id) { create(:todo_list).id }
        let(:id) { 100 }
        let(:task_updates) do
          {
            title: 'Title',
            description: 'Description'
          }
        end

        run_test!
      end
    end

    get 'retrieves a task' do
      tags 'Task'
      produces 'application/json'

      response '200', 'task updated' do
        schema '$ref' => '#/components/schemas/task_created_model'
        let(:todo_list_id) { create(:todo_list).id }
        let(:task) { create(:task, todo_list_id: todo_list_id) }
        let(:id) { task.id }

        run_test! do |response|
          response_body = JSON.parse(response.body)
          expect(response_body['id']).to eq(task.id)
          expect(response_body['title']).to eq(task.title)
          expect(response_body['description']).to eq(task.description)
        end
      end

      response '404', 'task not found' do
        schema '$ref' => '#/components/schemas/task_not_found_error'
        let(:todo_list_id) { create(:todo_list).id }
        let(:id) { 100 }
        let(:task_updates) do
          {
            title: 'Title',
            description: 'Description'
          }
        end

        run_test!
      end
    end
  end
end
