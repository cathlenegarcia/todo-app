# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

users = User.create([
  { first_name: 'Sheldon', last_name: 'Cooper' },
  { first_name: 'Leonard', last_name: 'Hofstadter' },
  { first_name: 'Rajesh', last_name: 'Koothrappali' },
  { first_name: 'Howard', last_name: 'Wolowitz' }
])

first_project = Project.create(name: 'String Theory', user: users.first)
second_project = Project.create(name: 'Lasers', user: users.second)
third_project = Project.create(name: 'Stellar Evolution', user: users.third)
fourth_project = Project.create(name: 'Space Toilet', user: users.fourth)

todo_list = TodoList.create(name: 'ToDo', project: first_project)
doing_list = TodoList.create(name: 'Doing', project: first_project)
done_list = TodoList.create(name: 'Done', project: first_project)

TodoList.create(name: 'ToDo', project: second_project)
TodoList.create(name: 'Doing', project: second_project)
TodoList.create(name: 'Done', project: second_project)

TodoList.create(name: 'ToDo', project: third_project)
TodoList.create(name: 'Doing', project: third_project)
TodoList.create(name: 'Done', project: third_project)

TodoList.create(name: 'ToDo', project: fourth_project)
TodoList.create(name: 'Doing', project: fourth_project)
TodoList.create(name: 'Done', project: fourth_project)

Task.create(title: 'Work on Math', todo_list: todo_list)
Task.create(title: 'Create a research paper', todo_list: todo_list)
Task.create(title: 'Research the topic', todo_list: todo_list)
Task.create(title: 'Make an outline', todo_list: todo_list)
Task.create(title: 'Start the research paper', todo_list: todo_list)
Task.create(title: 'Type out your spelling and grammar check', todo_list: todo_list)