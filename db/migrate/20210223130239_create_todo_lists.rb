class CreateTodoLists < ActiveRecord::Migration[6.0]
  def change
    create_table :todo_lists do |t|
      t.references :project, foreign_key: true, on_delete: :cascade
      t.string :name

      t.timestamps
    end
  end
end
