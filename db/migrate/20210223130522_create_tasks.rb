class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.references :todo_list, foreign_key: true, on_delete: :cascade
      t.string :title
      t.string :description
      t.integer :position

      t.timestamps
    end
  end
end
