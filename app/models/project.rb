class Project < ApplicationRecord
  belongs_to :user
  has_many :todo_lists, dependent: :destroy

  validates :name, presence: true
end
