class Task < ApplicationRecord
  belongs_to :todo_list
  acts_as_list scope: :todo_list

  validates :todo_list, presence: true
end
