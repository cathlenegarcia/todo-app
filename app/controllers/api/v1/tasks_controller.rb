module Api::V1
  class TasksController < ApplicationController
    before_action :set_todo_list
    before_action :set_task, only: [:show, :update, :destroy]

    def index
      @tasks = Task.where(todo_list_id: params[:todo_list_id]).order('position asc')
      render json: @tasks
    end

    def show
      render json: @task
    end

    def create
      @task = Task.new(task_params)

      if @task.save
        render json: @task
      else
        json_error(Errors.invalid_parameters(@task.errors))
      end
    end

    def update
      if @task.update(update_task_params)
        render json: @task
      else
        json_error(Errors.invalid_parameters(@task.errors))
      end
    end

    def destroy
      @task.destroy
    end

    private
      def set_todo_list
        @todo_list = TodoList.find_by(id: params[:todo_list_id])
        return @todo_list if @todo_list

        json_error(Errors.not_found('ToDo List'))
      end

      def set_task
        @task = Task.find_by(id: params[:id])

        return @task if @task

        json_error(Errors.not_found('Task'))
      end

      def task_params
        params.permit(:todo_list_id, :title, :description)
      end

      def update_task_params
        params.permit(:todo_list_id, :title, :description, :position)
      end
  end
end