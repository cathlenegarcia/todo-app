module Response
  def json_error(object)
    error = object[:error] || { status: '0009', message: 'Bad request.' }
    status = object[:status] || :bad_request
    render json: error, status: status
  end
end
