class Errors
  def self.not_found(record = 'record')
    {
      status: :not_found,
      error: {
        status: '0001',
        message: "#{record} not found."
      }
    }
  end

  def self.invalid_parameters(parameters)
    {
      status: :unprocessable_entity,
      error: {
        status: '0002',
        message: 'Missing/invalid parameters.',
        parameters: parameters
      }
    }
  end
end